/*  This loads an image file and generates a C formatted
    output. It's used to create the `image.c` that gets
    linked into `rre`.

    Copyright (c) 2016-2019 Charles Childers
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>

#ifndef BIT64
#define CELL int32_t
#define CELL_MIN INT_MIN + 1
#define CELL_MAX INT_MAX - 1
#else
#define CELL int64_t
#define CELL_MIN LLONG_MIN + 1
#define CELL_MAX LLONG_MAX - 1
#endif

uint8_t memory[512*1024*8];

CELL ngaLoadImage(char *imageFile) {
  FILE *fp;
  CELL imageSize;
  long fileLen;
  if ((fp = fopen(imageFile, "rb")) != NULL) {
    /* Determine length (in cells) */
    fseek(fp, 0, SEEK_END);
    fileLen = ftell(fp);
    rewind(fp);
    /* Read the file into memory */
    imageSize = fread(&memory, sizeof(uint8_t), fileLen, fp);
    fclose(fp);
  }
  else {
    printf("Unable to find the ngaImage!\n");
    exit(1);
  }
  return imageSize;
}

void output_header(int size) {
}

int main(int argc, char **argv) {
  int32_t size = 0;
  int32_t i;
  int32_t n;
  char spacer;

  if (argc == 2)
      size = ngaLoadImage(argv[1]);
  else
      size = ngaLoadImage("ngaImage");

  output_header(size);

  i = 0;
  n = 8;
  while (i < size) {
    if (n == 8) {
      printf("\n.byte");
      n = 0;
      spacer = ' ';
    }
    printf("%c0x%2.2x", spacer, memory[i]);
    spacer = ',';
    i++;
    n++;
  }
  exit(0);
}
