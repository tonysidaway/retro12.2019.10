// Copyright (c) 2017-2019 Charles Childers
// Copyright (c)      2019 Luke Parrish
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#if defined(_WIN16) || defined(_WIN32)
#include "strl.h"
#else
#include "unistd.h"
#include <sys/param.h>
#if !defined(BSD)
#include "strl.h"
#else
#define bsd_strlcpy strlcpy
#endif
#endif
#ifndef BIT64
#define CELL int32_t
#define CELL_MIN INT_MIN + 1
#define CELL_MAX INT_MAX - 1
#define ATOCELL atol
#else
#define CELL int64_t
#define CELL_MIN LLONG_MIN + 1
#define CELL_MAX LLONG_MAX - 1
#define ATOCELL atoll
#endif
#define KiB * 1024
#define MAX_NAMES 1024
#define STRING_LEN 64
#define IMAGE_SIZE 128 KiB
char Labels[MAX_NAMES][STRING_LEN];
char Defined[MAX_NAMES];
CELL Pointers[MAX_NAMES];
CELL np;
int address_unit = sizeof(CELL);
int data_size = sizeof(CELL);
int data_step;
uint8_t *target = NULL;
CELL here;
char image_file[256];
static inline uint8_t *targ_address (const CELL a) {
  return target + address_unit * a;
}
void cleanup () {
  if (target != NULL) free (target);
}
void save(const char *image_file) {
  FILE *fp;
  if ((fp = fopen(image_file, "wb")) == NULL) {
    printf("Unable to save the image to %s!\n",
      image_file);
    cleanup ();
    exit(2);
  }
  fwrite(target, address_unit, here, fp);
  fclose(fp);
}
static inline void expressbytes (uint8_t *where, const size_t nbytes, CELL what) {
size_t i;
    for (i = 0; i < nbytes; ++i) {
	*(where + i) = (uint8_t)(what & 0xff);
	what = what >> 8;
    }
}
static inline CELL retrievebytes (uint8_t *where, const size_t nbytes) {
size_t i;
CELL val = 0;
    for (i = 0; i < nbytes; ++i) {
	val += (*(where + i) << (8 * i));
    }
    return val;
}
static inline CELL lookup(const char *name) {
  CELL slice = -1;
  CELL n = np;
  while (n > 0) {
    n--;
    if (strcmp(Labels[n], name) == 0)
      slice = n;
  }
  return slice;
}
static inline CELL create_label (const char *name) {
  bsd_strlcpy(Labels[np], name, STRING_LEN);
  Defined[np] = 0;
  return np++;
}
void add_label (const char *name, int defined, CELL value) {
  CELL p;
  CELL link;
  CELL next;
  CELL lkp = lookup(name);
  if (!defined) {
    if (lkp == -1) {
      lkp = create_label (name);
      Defined[lkp] = 0;
      p = here + 1;
    } else {
      p = Pointers[lkp];
    }
    expressbytes(targ_address (here), data_size, p);
    if (!Defined[lkp]) {
      Pointers[lkp] = here;
    }
    return;
  }
  
  if (lkp == -1) {
    lkp = create_label(name);
    Defined[lkp] = 1;
    Pointers[lkp] = value;
    expressbytes (targ_address (here), data_size, value);
    return;
  }
  if (Defined[lkp]) {
    printf("Fatal error: %s already defined\n", name);
    cleanup ();
    exit(1);
  }
  Defined[lkp] = 1;
  link = Pointers[lkp];
  Pointers[lkp] = value;
  while (next = retrievebytes (targ_address (link), data_size),
	 expressbytes (targ_address (link), data_size, value),
	 next < link){
    link = next;
  }
}
void read_line(FILE *file, char *line_buffer) {
  int ch = getc(file);
  int count = 0;
  while ((ch != '\n') && (ch != EOF)) {
    line_buffer[count] = ch;
    count++;
    ch = getc(file);
  }
  line_buffer[count] = '\0';
}
CELL opcode_for(char *s) {
  static char* opcodeList = "..lidudrswpupojucaccreeqneltgtfestadsumudianorxoshzrenieiqii";
  int16_t* s16 = (int16_t *)s;
  int16_t* op16 = (int16_t *)opcodeList;
  int i = 0;
  for(i = 0; i <= 30; i++){
    if(s16[0] == op16[i]){
      return i;
    }
  }
  return 0;
}
void assemble_lines (FILE *fp, const int quota) {
  int inBlock = 0;
  int done = 0;
  CELL cellval;
  CELL pointer;
  char buffer [1 KiB];
  CELL opcode;
  char inst[3];
  int byt;
  int slot;
  int lits;
  int ops;
  uint8_t c;
  inst[2] = '\0';
  if (quota > 0) inBlock = 1;
  while (!feof(fp)) {
    if ((quota > 0) && (done >= quota)) return;
    read_line(fp, buffer);
    if (strcmp(buffer, "~~~") == 0) {
      if (inBlock == 0)
        inBlock = 1;
      else
        inBlock = 0;
    } else {
      if (inBlock == 1) {
        if (buffer[1] == '\t' || buffer[1] == ' ') {
          switch (buffer[0]) {
            case 'i':
		      slot = 0;
		      lits = 0;
		      ops = 0;
		      opcode = 0;
		      for (byt = 0; byt < 4; ++byt) {
		        memcpy (inst, buffer + 2 + byt * 2, 2);
                        c = opcode_for(inst);
			if (c == 1) lits++;
                        opcode += c << (8 * slot);
			slot++;
			if (slot == address_unit) {
			  // Skip nop-only sequences after first byte in group
			  if ((opcode || !ops)) {
		            expressbytes (targ_address (here), address_unit, opcode);
			    here++;
			    ops++;
			  }
			  opcode = 0;
			  slot = 0;
			  if (lits > 0) {
			    assemble_lines (fp, lits);
			    lits = 0;
			  }
			  done++;
			}
		      }
 
                      break;
            case 'r':
	      add_label (buffer+2, 0, 0);
	      here += data_step;
	      done++;
                      break;
            case 'd': cellval = ATOCELL(buffer+2);
	      expressbytes (targ_address (here), data_size, cellval);
		      here += data_step;
		      done++;
                      break;
            case 's':
            	      opcode = 2;
                      while (opcode < strlen(buffer)) {
			cellval = buffer[opcode];
		        expressbytes (targ_address (here), address_unit, cellval);
			here++;
			opcode++;
		      }
		      expressbytes (targ_address (here), address_unit, 0);
		      done++;
		      here++;
                      break;
            case ':':
	    	      add_label(buffer+2, 1, here);
		      done++;
                      break;
          }
        }
      }
    }
  }
}
  
void assemble_file(char *fname) {
  FILE *fp;
  fp = fopen(fname, "r");
  if (fp == NULL) {
    printf("Unable to load file\n");
    cleanup ();
    exit(2);
  }
  here = 0;
  assemble_lines (fp, 0);
  fclose(fp);
}
void usage (char *program_name) {
#if !defined(_WIN16) && !defined(_WIN32)
  printf("muri\n(c) 2017-2019 charles childers, (c) 2019 luke parrish\n\n%s [-a address_unit] [-d data_size] [-o output_file] filename\nAll units are in bytes\n", program_name);
#else
  printf("muri\n(c) 2017-2019 charles childers, (c) 2019 luke parrish\n\n%s filename\n", program_name);
#endif
}
void parse_options (int argc, char **argv) {
#if !defined(_WIN16) && !defined(_WIN32)
int opt;
  while (opt = getopt (argc, argv, ":a:d:o:"),
  	opt != -1) {
    switch (opt) {
    default:
      usage (argv[0]);
      exit (1);
      break;
    case 'a':
      address_unit = atoi (optarg);
      break;
    case 'd':
      data_size = atoi (optarg);
      break;
    case 'o':
      bsd_strlcpy(image_file, optarg, sizeof(image_file));
      break;
    }
  }
#endif
}
#if defined(_WIN16) || defined(_WIN32)
const int optind = 1;
#endif
int main(int argc, char **argv) {
  bsd_strlcpy(image_file, "ngaImage", sizeof(image_file));
  parse_options (argc, argv);
  data_step = data_size / address_unit;
  np = 0;
  if (argc > optind) {
    target = (uint8_t *)malloc (IMAGE_SIZE * address_unit);
    assemble_file(argv[optind]);
    save(image_file);
    printf("Wrote %lld cells to %s\n", (long long)here, image_file);
    cleanup();
  }
  else
    usage (argv[0]);
  return 0;
}
